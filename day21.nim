# https://adventofcode.com/2020/day/21

import os, strutils, tables, sequtils, algorithm

if paramCount() < 1:
  quit(1)
  
type
  ListItem = object
    ingredients, allergens: seq[string]
var
  data: seq[ListItem] = @[]
  allIngredients: seq[string] = @[]
  allAllergens: seq[string] = @[]
  possibilities: Table[string, seq[string]] = initTable[string, seq[string]]()

# We use an approach similar to the one used at
# https://github.com/rHermes/adventofcode/blob/master/2020/21/y2020_d21_p01.py
# The underlying assumption here is that if an allergen is mentioned in
# multiple lines, it must be in one of the items common to all lines,
# While this cannot help us to pinpoint exactly which ingredient contains
# which allergen, it can be used to identify those that do not.
block part1:
  var
    parts, ingredients: seq[string]
    count: int = 0
    hasNoAllergens: bool
    
  for line in paramStr(1).readFile.strip().splitLines():
    parts = line.split(" (contains ")
    data.add(ListItem(ingredients: parts[0].split(" "), 
      allergens: parts[1][0 .. ^2].split(", ")))
    
    for ingredient in data[^1].ingredients:
      if ingredient notin allIngredients:
        allIngredients.add(ingredient)
        
    for allergen in data[^1].allergens:
      if allergen notin allAllergens:
        allAllergens.add(allergen)
    
      if not possibilities.hasKey(allergen):
        possibilities[allergen] = @[]
        possibilities[allergen].add(data[^1].ingredients)
      else:
        ingredients = @[]
        for ingredient in data[^1].ingredients:
          if ingredient in possibilities[allergen]:
            ingredients.add(ingredient)
        possibilities[allergen] = @[]
        possibilities[allergen].add(ingredients)
  
  for ingredient in allIngredients:
    hasNoAllergens = true
    for allergen, candidates in possibilities.pairs():
      if ingredient in candidates:
        hasNoAllergens = false
        break
    
    if not hasNoAllergens:
      continue
    
    for entry in data:
      if ingredient in entry.ingredients:
        count += 1
        
  echo "Part 1: ", count
  
# Here we need to use a process of elimination to match the ingredient to the
# allergen. The starting point is the list of candidates obtained earlier.
# Here, the assumption is that at allergen in at least one ingrdient can be
# directly identified from the list of candidates.
block part2:
  var
    completed: bool = false
    tested: seq[string] = @[]
    candidateToRemove: string
    answer: string
    
  while not completed:
    for allergen, candidates in possibilities.pairs():
      if candidates.len() == 1 and candidates[0] notin tested: # Identified
        candidateToRemove = candidates[0]
        tested.add(candidateToRemove)
        break
    
    for allergen in possibilities.keys():
      if possibilities[allergen].len() > 1 and 
        candidateToRemove in possibilities[allergen]:
        possibilities[allergen] = possibilities[allergen].filterIt( it != candidateToRemove )
    
    completed = true
    for allergen, candidates in possibilities:
      if candidates.len() > 1:
        completed = false
        break
  
  for allergen in allAllergens.sorted():
    if answer.len() == 0:
      answer = possibilities[allergen][0]
    else:  
      answer = answer & "," & possibilities[allergen][0]
  
  echo "Part 2: ", answer