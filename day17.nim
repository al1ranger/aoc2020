# https://adventofcode.com/2020/day/17

import os, strutils, tables

if paramCount() < 1:
  quit(1)

type
  Values = tuple[old, new: bool] # This avoids coopying the entire table
  Line = Table[int, Values] # W Axis
  Plane = Table[int, Line] # X Axis
  Grid = Table[int, Plane] # Y Axis
  Grid4d = Table[int, Grid] # Z Axis
  
# Set tne new status as the old status
proc copyStatus(grid: var Grid4d) =
  for z in grid.mvalues():
    for y in z.mvalues():
      for x in y.mvalues():
        for w in x.mvalues():
          w.old = w.new

# Returns status 
proc getStatus(grid: var Grid4d, x, y, z: int, w:int, sync:bool = false): bool =
  result = false
  
  if grid.haskey(z) and grid[z].haskey(y) and grid[z][y].hasKey(x) and 
    grid[z][y][x].hasKey(w):
    if sync:
      grid[z][y][x][w].old = grid[z][y][x][w].new
      
    result = grid[z][y][x][w].old

# Sets the status        
proc setStatus(grid: var Grid4d, x, y, z, w: int, status:bool) =
  if not grid.hasKey(z):
    grid[z] = initTable[int, Plane]()
  
  if not grid[z].hasKey(y):
    grid[z][y] = initTable[int, Line]()
  
  if not grid[z][y].hasKey(x):
    grid[z][y][x] = initTable[int, Values]()
  
  if not grid[z][y][x].hasKey(w):
    grid[z][y][x][w] = (old: false, new: false)
  
  grid[z][y][x][w].new = status

# Reads data and initializes the grid
proc initializeGrid(grid: var Grid4d) =
  grid = initTable[int, Grid]()
  
  var
    x:int = 0
    y:int = 0
  
  for line in paramStr(1).readFile().strip().splitLines():
    x = 0
    for char in line:
      if char == '#':
        grid.setStatus(x, y, 0, 0, true)
      
      x += 1
    y += 1

# Simultes a cycle and returns the active cubes  
proc simulateCycle(grid: var Grid4d, used4d: bool): int =
  var
    fromX, toX, fromY, toY, fromZ, toZ, fromW, toW, fromDw, toDw, w: int
    active: int
    status: bool
    
  result = 0
  grid.copyStatus()
  
  for z in grid.keys():
    if z < fromZ:
      fromZ = z
    
    if z > toZ:
      toZ = z
    
    for y in grid[z].keys():
      if y < fromY:
        fromY = y
      
      if y > toY:
        toY = y
    
      for x in grid[z][y].keys():
        if x < fromX:
          fromX = x
        
        if x > toX:
          toX = x
        
        if used4d:
          for w in grid[z][y][x].keys():
            if w < fromW:
              fromW = w
    
            if w > toW:
              toW = w
        else:
          w = 0
  
  if used4d:
    fromDw = -1
    toDw = 1
  else:
    fromDw = 0
    toDw = 0
  
  for z in fromZ - 1 .. toZ + 1:
    for y in fromY - 1 .. toY + 1:
      for x in fromX - 1 .. toX + 1:
        for w in fromW + fromDw .. toW + toDw:
          status = grid.getStatus(x, y, z, w)
          active = 0
        
          for dz in -1 .. 1:
            for dy in -1 .. 1:
              for dx in -1 .. 1:
                for dw in fromDw .. toDw:
                  if dx == 0 and dy == 0 and dz == 0 and dw == 0:
                    continue
              
                  if grid.getStatus(x + dx, y + dy, z + dz, w + dw):
                    active += 1
        
          case status
          of true:
            if active < 2 or active > 3:
              status = false
          of false:
            if active == 3:
              status = true
          
          grid.setStatus(x, y, z, w, status)
          if status:
            result += 1

# Simulates multiple cycles and returns the active cubes in the last cycle
proc simulateCycles(cycles: int, use4d: bool): int =
  result = 0
  
  var
    grid: Grid4d
  
  grid.initializeGrid()
  for i in 1 .. cycles:
    result = grid.simulateCycle(use4d)  

# Three dimensional simulation              
block part1:
  echo "Part 1: ", simulateCycles(6, false) 

# Four dimensional simulation
block part2:
  echo "Part 2: ", simulateCycles(6, true)  