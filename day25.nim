# https://adventofcode.com/2020/day/25

import os, strutils

if paramCount() < 1:
  quit(1)
  
type
  KeyData = object
    public, private: BiggestInt 
    loops: int

var
  door, keyCard: KeyData
  
block part1:
  let
    data = paramStr(1).readFile().strip().splitLines()
  
  const
    subject = 7
  
  var
    value: int = 1
    loops: int
  
  door.public = data[0].parseInt()
  keyCard.public = data[1].parseInt()
  
  while value != keyCard.public:
    value *= subject
    value = value mod 20201227
    
    keyCard.loops += 1
  
  loops = 0
  keyCard.private = 1
  while loops < keyCard.loops:
    keyCard.private *= door.public
    keyCard.private = keyCard.private mod 20201227
    loops += 1
    
  echo "Part 1: ", keyCard.private