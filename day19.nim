# https://adventofcode.com/2020/day/19

import os, strutils, pegs, tables

if paramCount() < 1:
  quit(1)

type
  RuleData = tuple[value: char, children: seq[seq[int]]]
  Rules = Table[int, RuleData]
  Repeats = Table[int, int]

# As mentioned in the subreddit post given below, the loops can be manually
# expanded to give a repeating sequence of rules
# https://www.reddit.com/r/adventofcode/comments/kg1mro/2020_day_19_solutions/ggc24v2/

# Essentially,
# Rule 8 = Rule 42 | Rule 42 Rule 42 | ...
# Rule 11 = Rule 42 Rule 31 | Rule42 Rule42 Rule31 Rule31 | ...

# We will base the solution on the approach employed here
# https://github.com/sophiebits/adventofcode/blob/main/2020/day19.py
# The problem with specifying the repetitions as alternatives in PEGs is that
# it may match more or less than what is required.
# For this, we pass repetition counts to decide the number of repeats required
# in the PEG.

# Create a PEG based on the rules and repetition counts
# Documentation: https://nim-lang.org/docs/pegs.html 
proc buildPeg(rules: Rules, repeats:Repeats = initTable[int, int]()): Peg = 
  var
    pegStr:string = "Result <- ^Rule0$ \n" # Rule zero is the root
    times:int
  
  # Add the other rules
  for ruleNo, rule in rules.pairs():
    pegStr = pegStr & "Rule" & $ruleNo & " <- "
    
    if rule.value.isAlphaAscii():
      pegStr = pegStr & "'" & rule.value & "' \n"
      continue
      
    if repeats.hasKey(ruleNo) and repeats[ruleNo] > 1:
      times = repeats[ruleNo]
    else:
      times = 1
    
    for i in rule.children.low() .. rule.children.high():
      var
        rulePeg: string
        ruleName: string
      
      if rule.children[i].len() == 0:
        continue
      
      if i == rule.children.low():
        rulePeg = " "
      else:
        rulePeg = " / "
      
      for rNo in rule.children[i]:
        ruleName = "Rule" & $rNo & ' '
        rulePeg = rulePeg & ruleName.repeat(times)
      
      pegStr = pegStr & rulePeg
    
    pegStr = pegStr & '\n'
    
  result = peg pegStr

var
  ruleSet: Rules = initTable[int, RuleData]()
  messages: seq[string] = @[]
  
block part1:
  let parser = peg """
    rule <- {ruleNo} ': ' (ruleSets+ / value)
    ruleNo <- \d+
    ruleSet <- {ruleNo} \s*
    orClause <- {'| '} ruleSet+
    ruleSets <- ruleSet orClause*
    value <- '"' {\a+} '"'
  """

  var
    section: int = 0 # 0 -> rules, 1 -> values
    validator: Peg
    count:int = 0
  
  for line in paramStr(1).readFile().splitLines():
    var
      ruleNo: int
    
    if line.len() == 0:
      section += 1
      validator = ruleSet.buildPeg()
      continue
      
    if section == 0:
      if line =~ parser:
        ruleNo = matches[0].parseInt()
        ruleSet[ruleNo] = (value: ' ', children: newSeq[seq[int]]())
        
        for i in 1 .. matches.high():
          if matches[i].len() == 0:
            break
          
          if matches[i][0].isAlphaAscii():
            ruleSet[ruleNo].value = matches[i][0]
          elif matches[i][0] == '|':
            ruleSet[ruleNo].children.add(@[])
          else:
            if ruleSet[ruleNo].children.len() == 0:
              ruleSet[ruleNo].children.add(@[])
            
            ruleSet[ruleNo].children[^1].add(matches[i].parseInt())
    else:
      if line.len() > 0:
        messages.add(line)
        if line =~ validator:
          count += 1

  echo "Part 1: ", count

# Create a PEG by varying the repetition counts for rules 8 and 11 and use it
# to check messages that have not passed previous checks 
block part2:
  var
    validator: Peg
    valid:seq[string] = @[]
    repeats: Repeats = initTable[int, int]()
  
  # Vary the limits as required. From the subreddit discussion, a maximum 
  # value of 100 seems to be sufficient for almost all input scenarios.
  for i in 1 .. 25:
    for j in 1 .. 25:
      repeats[8] = i
      repeats[11] = j
      
      validator = ruleSet.buildPeg(repeats)
      for message in messages:
        if message in valid:
          continue
    
        if message =~ validator:
          valid.add(message)

  echo "Part 2: ", valid.len()