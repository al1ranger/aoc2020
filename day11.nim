# https://adventofcode.com/2020/day/11

import os, strutils, sugar

if paramCount() < 1:
  quit(1)

proc lineOfSightOccupied(layout: seq[string], row, col: int, 
  onlyAdjacent:bool = true): int =
  result = 0
  
  var
    r, c: int
  
  for rowInc in -1 .. 1:
    for colInc in -1 .. 1:
      if rowInc == 0 and colInc == 0: # This is the cell being checked
        continue
        
      r = row + rowInc
      c = col + colInc
      
      while r >= layout.low() and r <= layout.high() and 
        c >= 0 and c < layout[r].len():
        if layout[r][c] != '.': # Only the first seat is to be considered
          if layout[r][c] == '#':
            result += 1
          break
        
        if onlyAdjacent: # The while loop needs to run only once
          break
        
        r += rowInc
        c += colInc

proc calculateOccupiedSeats(layout: seq[string], occupied: int, 
  onlyAdjacent: bool = true): int =
    var
      currentLayout: seq[string] = layout
      newLayout: seq[string]
      stateChanged: bool
    
    result = 0 # All seats are initially empty
  
    stateChanged = true
    while stateChanged:
      stateChanged = false
      newLayout = currentLayout
    
      for row in currentLayout.low() .. currentLayout.high():
        for col in 0 ..< currentLayout[row].len():
          case currentLayout[row][col]:
            of 'L':
              if currentLayout.lineOfSightOccupied(row, col, onlyAdjacent) == 0:
                newLayout[row][col] = '#'
                stateChanged = true
                result += 1
            of '#':
              if currentLayout.lineOfSightOccupied(row, col, onlyAdjacent) >= 
                occupied:
                newLayout[row][col] = 'L'
                stateChanged = true
                result -= 1
            else:
              discard
      
      currentLayout = newLayout

# Read the starting seating arrangement
let  
  seatsLayout: seq[string] = collect(newSeq):
    for line in readFile(os.paramStr(1)).strip().splitLines():
      line

block part1:
  echo "Part 1: ", seatsLayout.calculateOccupiedSeats(4)
  
# This can be considered as a more general case of Part 1, where the gaps 
# between seats are also to be considered
block part2:
  echo "Part 2: ", seatsLayout.calculateOccupiedSeats(5, false)