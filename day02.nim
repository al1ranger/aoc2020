# https://adventofcode.com/2020/day/2

import os, strutils, strscans

if paramCount() < 1:
  quit(1)

type
  Password = tuple[min, max: int, symbol: char, password: string]

var
  passwords: seq[Password] = @[]

block part1:
  let 
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  var
    min, max, valid, times: int
    symbol, password: string
  
  valid = 0  
  for line in data:
    if scanf(line, "$i-$i$s$w:$s$w", min, max, symbol, password):
      passwords.add((min:min, max:max, symbol:symbol[0], password:password))
      
      times = password.count(symbol) 
      if times >= min and times <= max:
        valid += 1
  echo "Part 1: ", valid
  
block part2:
  var
    valid: int
  
  valid = 0  
  for password in passwords:
      if (password.password[password.min - 1] == password.symbol and 
      password.password[password.max - 1] != password.symbol) or 
      (password.password[password.min - 1] != password.symbol and 
      password.password[password.max - 1] == password.symbol):
        valid += 1
  echo "Part 2: ", valid