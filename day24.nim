# https://adventofcode.com/2020/day/24

import os, strutils, pegs, tables

if paramCount() < 1:
  quit(1)

# This is a useful reference: https://www.redblobgames.com/grids/hexagons/
# In hexadecimal grids, the diagonal distance is half of the horizontal 
# distance. For convenience, we will take the horizontal distance between
# centres as 2 units and the vertical distance as 1 unit.

type
  TileMap = object
    map: Table[int, Table[int, bool]]
    minX, minY, maxX, maxY: int

proc getAt(map: TileMap, x, y: int): bool =
  result = false
  
  if map.map.hasKey(x) and map.map[x].hasKey(y):
    result = map.map[x][y]

# Storing only the black tiles improves performance 
proc setAt(map: var TileMap, x, y: int, flag: bool) =
  if x < map.minX:
    map.minX = x
  elif x > map.maxX:
    map.maxX = x
  
  if y < map.minY:
    map.minY = y
  elif y > map.maxY:
    map.maxY = y
  
  if flag:
    if not map.map.hasKey(x):
      map.map[x] = initTable[int, bool]()
    map.map[x][y] = flag
  else:
    if map.map.hasKey(x):
      map.map[x].del(y)

proc countBlack(map: TileMap): int =
  result = 0
  
  for x, ys in map.map.pairs():
    result += ys.len()

var
  tileMap: TileMap
  
block part1:
  let 
    parser: Peg = peg """
    input <- {direction}
    direction <- 'se' / 'sw' / 'ne' / 'nw' / 'e' / 'w'
  """

  proc flipTile(tileMap: var TileMap, directions: string) =
    var
      x: int = 0
      y: int = 0
    
    for match in directions.findAll(parser):
      case match:
      of "e":
        x += 2
      of "w":
        x -= 2
      of "nw":
        x -= 1
        y += 1
      of "ne":
        x += 1
        y += 1
      of "sw":
        x -= 1
        y -= 1
      of "se":
        x += 1
        y -= 1
      else:
        discard
  
    tileMap.setAt(x, y, not tileMap.getAt(x, y))
  
  let
    data = paramStr(1).readFile().strip().splitLines()
  
  tileMap.map = initTable[int, Table[int, bool]]()  
    
  for line in data:
    tileMap.flipTile(line)
  
  echo "Part 1: ", tileMap.countBlack()

# Here, we need to expand the limits to account for tiles on the edges
block part2:
  proc adjacentBlack(map: TileMap, x, y: int): int =
    const
      dx: seq[int] = @[-2, -1, -1, 1, 1, 2]
      dy: seq[int] = @[0, -1, 1, 1, -1, 0]
      
    result = 0
    
    var
      finalX, finalY: int
      
    for i in 0 .. 5:
      finalX = x + dx[i]
      finalY = y + dy[i]
      
      if map.getAt(finalX, finalY):
        result += 1
   
  proc flipTilesForday(map: TileMap): TileMap =
    result.map = initTable[int, Table[int, bool]]()
    
    var
      adjacentBlack: int
      newFlag: bool
    
    result.minY = map.minY - 1
    result.minX = map.minX - 2
    result.maxY = map.maxY + 1
    result.maxX = map.maxX + 2
    
    for x in countup(result.minX, result.maxX, 1):
      for y in countup(result.minY, result.maxY, 1):
        adjacentBlack = map.adjacentBlack(x, y)
        newFlag = map.getAt(x, y)
        
        case newFlag
        of true:
          if adjacentBlack == 0 or adjacentBlack > 2:
            newFlag = false
        of false:
          if adjacentBlack == 2:
            newFlag = true
        
        result.setAt(x, y, newFlag)

  var
    initialMap: TileMap = tileMap
  
  for i in 1 .. 100:
    initialMap = initialMap.flipTilesForday()
  
  echo "Part 2: ", initialMap.countBlack()
