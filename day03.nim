# https://adventofcode.com/2020/day/3

import os, strutils

if paramCount() < 1:
  quit(1)

let 
  data = readFile(os.paramStr(1)).strip().splitLines()
  rows = data.len()
  cols = data[0].len()

proc countTrees(vx, vy: int): int =
  var
    x, y: int
  
  x = 0 # column
  y = 0 # row
  result = 0
  
  while y < rows:
    if data[y][x] == '#':
      result += 1
    
    x += vx
    x = x mod cols # as the pattern repeats
    y += vy

block part1:
  echo "Part 1: ", countTrees(3, 1)  
  
block part2:
  type
    Speed = tuple[vx, vy: int]
  
  let speeds: array[5, Speed] = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
  
  var trees: int = 1
  for speed in speeds:
    trees *= countTrees(speed.vx, speed.vy)
  
  echo "Part 2: ", trees