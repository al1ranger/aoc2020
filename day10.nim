# https://adventofcode.com/2020/day/10

import os, strutils, algorithm

if paramCount() < 1:
  quit(1)

const
  OutletJolts: int = 0

var
  adapters: seq[int] = @[]

# Here, complete data needs to be read before prcessing
block readInput:
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  for line in data:
    adapters.add(line.parseInt())
  
  # Add the outlet
  adapters.add(OutletJolts)
  
  adapters.sort()
  
  # Add the last Jolt to the device power
  adapters.add(adapters[^1] + 3)
  
# This is essentially counting the types of intervals in the sorted list  
block part1:  
  var
    oneJoltDiffs, threeJoltDiffs, currentAdapter: int
  
  oneJoltDiffs = 0
  threeJoltDiffs = 0
  currentAdapter = adapters[0]
  
  for i in 1 .. adapters.high():
    case adapters[i] - currentAdapter:
      of 1:
        oneJoltDiffs += 1
      of 3:
        threeJoltDiffs += 1
      else:
        oneJoltDiffs = 0
        threeJoltDiffs = 0
        break
    
    currentAdapter = adapters[i]
  
  echo "Part 1: ", oneJoltDiffs * threeJoltDiffs

# This was particularly useful in identifying the number of variable segments:
# https://forum.nim-lang.org/t/7162#45716

# Here we need to find all possible arrangements. 

# Instead of a brute force approach, the performance can be drastically improved
# if we identify those sets of adapters which can have different arrangements.
# For the sorted list of adapters, these sectons will be indicated by a gap of
# three Jolts between consecutive items.
block part2:
  # The index of the last adapter checked determines the starting point for next 
  # step in the recursive search. This approach works as we only need only the 
  # number of arrangemnts and not the arrangement.
  proc findPossiblePaths(arrangement: seq[int], lastIndex: int = 0): 
    BiggestInt =
    
    result = 0
    
    var
      joltDiff, lastAdapter: int
    
    if lastIndex >= arrangement.high(): # Reached the end
      result = 1
      return
    
    lastAdapter = arrangement[lastIndex]
    
    for i in lastIndex + 1 .. arrangement.high():
      joltDiff = arrangement[i] - lastAdapter
      if joltDiff >= 1 and joltDiff <= 3:
        result += arrangement.findPossiblePaths(i)
      else: # As list is sorted, no other possibilities exist
        break
  
  var
    variablePath: seq[int] = @[adapters[adapters.low()]]
    totalPaths: BiggestInt = 1
    
  for i in adapters.low() + 1 .. adapters.high():
    variablePath.add(adapters[i])
    
    # Check if we have a complete section which can have multiple arrangements
    if i == adapters.high() or adapters[i + 1] - adapters[i] == 3:
      totalPaths *= variablePath.findPossiblePaths()
      variablePath = @[]
      
  echo "Part 2: ", totalPaths
