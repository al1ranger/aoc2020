# https://adventofcode.com/2020/day/5

import os, strutils, algorithm

# calculate the seat id from the rules for interpretng the boarding pass id
proc determineSeatId(passId: string): int =
  result = 0
  
  var
    minRow, maxRow, minCol, maxCol, row, col: int
    
  minRow = 0
  maxRow = 127
  minCol = 0
  maxCol = 7
  
  for id in passId:
    case id:
      of 'F':
        maxRow = maxRow - int((maxRow - minRow + 1) / 2)
        row = minRow
      of 'B':
        minRow = minRow + int((maxRow - minRow + 1) / 2)
        row = maxRow
      of 'L':
        maxCol = maxCol - int((maxCol - minCol + 1) / 2)
        col = minCol
      of 'R':
        minCol = minCol + int((maxCol - minCol + 1) / 2)
        col = maxCol
      else:
        discard
  
  result = row * 8 + col
  
if paramCount() < 1:
  quit(1)
  
var
  seatIds: seq[int] = @[]

block part1:
  let 
    data = readFile(os.paramStr(1)).strip().splitLines()

  var
    maxSeatId, currentSeatId: int
  
  maxSeatId = 0
  for passId in data:
    currentSeatId = determineSeatId(passId)
    seatIds.add(currentSeatId)
    
    if currentSeatId > maxSeatId:
      maxSeatId = currentSeatId
  
  echo "Part 1: ", maxSeatId
  
block part2:
  var
    sortedIds: seq[int]
    lastSeatId: int
  
  # Sort the list of seat ids, the missing id is our seat
  sortedIds = seatIds.sorted()
  lastSeatId = -1
  for seatId in sortedIds:
    if lastSeatId == -1:
      lastSeatId = seatId
      continue
    
    if seatId - lastSeatId == 2: # a seat is missing
      lastSeatId += 1
      break
      
    lastSeatId = seatId
  
  echo "Part 2: ", lastSeatId  