# https://adventofcode.com/2020/day/22

import os, strutils, deques

if paramCount() < 1:
  quit(1)

type
  Draws = array[2, Deque[int]]

var
  draws: Draws = [initDeque[int](), initDeque[int]()]
  
block readDraws:
  var
    currentPlayer: int = -1
  for line in paramStr(1).readFile().strip().splitLines():
    if line.len() == 0:
      continue
      
    if line.len() > 7 and line[0 .. 6] == "Player ":
      currentPlayer += 1
      continue
      
    draws[currentPlayer].addLast(line.parseInt())

# Double ended queues can be used to easily model the game rules  
block part1:
  var
    decks: Draws = draws
    topPlayer1, topPlayer2, score, winner: int
  
  while decks[0].len() > 0 and decks[1].len() > 0:
    topPlayer1 = decks[0].popFirst()
    topPlayer2 = decks[1].popFirst()
    
    if topPlayer1 > topPlayer2:
      decks[0].addLast(topPlayer1)
      decks[0].addLast(topPlayer2)
    elif topPlayer2 > topPlayer1:
      decks[1].addLast(topPlayer2)
      decks[1].addLast(topPlayer1)
  
  if decks[0].len() > 0:
    winner = 0
  else:
    winner = 1
  
  for i, value in decks[winner].pairs():
    score += value * (decks[winner].len() - i)
  
  echo "Part 1: ", score

# Here, for the recursive combat, a turn history needs to be maintained  
block part2:
  proc contains(turns: seq[Draws], draws: Draws): bool =
    result = false
    
    for turn in turns:
      if (draws[0].len() != turn[0].len()) or (draws[1].len() != turn[1].len()):
        continue
      
      result = true
      for i, value in turn[0].pairs():
        if value != draws[0][i]:
          result = false
          break
        
      result = true
      for i, value in turn[1].pairs():
        if value != draws[1][i]:
          result = false
          break
      
      if result:
        break
  
  proc recursiveCombat(draws: var Draws): int =
    result = -1
    
    var
      turnHistory: seq[Draws] = @[]
      decks: Draws
      topPlayer1, topPlayer2, winner: int
      
    while draws[0].len() > 0 and draws[1].len() > 0:
      if turnHistory.contains(draws):
        result = 0
        return
      
      turnHistory.add(draws) # Save current turn data
      
      topPlayer1 = draws[0].popFirst()
      topPlayer2 = draws[1].popFirst()
      
      # Check whether a sub-game needs to be triggered
      if draws[0].len() < topPlayer1 or draws[1].len() < topPlayer2:
        if topPlayer1 > topPlayer2:
          winner = 0
        elif topPlayer2 > topPlayer1:
          winner = 1
        else:
          continue
      else:
        decks[0] = initDeque[int]()
        for i in 0 ..< topPlayer1:
          decks[0].addLast(draws[0][i])
        
        decks[1] = initDeque[int]()
        for i in 0 ..< topPlayer2:
          decks[1].addLast(draws[1][i])  
          
        winner = decks.recursiveCombat()
      
      # Update the hands of the winner  
      if winner == 0:
        draws[winner].addLast(topPlayer1)
        draws[winner].addLast(topPlayer2)
      else:
        draws[winner].addLast(topPlayer2)
        draws[winner].addLast(topPlayer1)
    
    result = winner
      
  var
    decks: Draws = draws
    winner: int
    score: int = 0
  
  winner = decks.recursiveCombat()
  
  for i, value in decks[winner].pairs():
    score += value * (decks[winner].len() - i)
  
  echo "Part 2: ", score