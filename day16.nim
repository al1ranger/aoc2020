# https://adventofcode.com/2020/day/16

import os, strutils, tables, sequtils

if paramCount() < 1:
  quit(1)

type
  Range = tuple[low, high: int]
  
var
  rules: Table[string, seq[Range]] = initTable[string, seq[Range]]()
  ticket: seq[int] = @[]
  otherTickets: seq[seq[int]] = @[]

# A ticket is invalid if it fails all the ranges of the rule
block part1:
  var
    section: int = 0 # 0 -> rules, 1 -> own ticket, 2 -> other tickets
    data, ruleParts, limits: seq[string]
    invalid, currentTicket: seq[int]
    errorRate: int = 0
  
  for line in paramStr(1).readFile().strip().splitLines():
    if line.len() == 0 or line == "":
      continue
    elif line == "your ticket:":
      section = 1
      continue
    elif line == "nearby tickets:":
      section = 2
      continue
    
    case section
    of 0:
      data = line.split(": ")
      rules[data[0]] = @[]
      ruleParts = data[1].split(" or ")
      for part in ruleParts:
        limits = part.split("-")
        rules[data[0]].add((low: limits[0].parseInt(), 
        high: limits[1].parseInt()))
    of 1:
      data = line.split(",")
      for value in data:
        ticket.add(value.parseInt())
    of 2:
      data = line.split(",")
      currentTicket = @[]
      for value in data:
        currentTicket.add(value.parseInt())
    else:
      discard
      
    if section != 2:
      continue
    
    invalid = @[] # Values that are invalid
    for value in currentTicket:
      block checkTicket:
        for name, ranges in rules.pairs():
          for range in ranges:
            if value >= range.low and value <= range.high:
              break checkTicket
        
        if value notin invalid:
          invalid.add(value)
    
    if invalid.len() > 0:
      for value in invalid:
        errorRate += value
    else:
      otherTickets.add(@[])
      otherTickets[^1] = currentTicket
    
  echo "Part 1: ", errorRate

# This is a constraint satisfaction problem
# https://en.wikipedia.org/wiki/Constraint_satisfaction
# Here we need to use a process of elimination to identify the columns.
# The nearby tickets in the data can be used to fix mapping for some fields.
# This can then be used to fix the mapping for the remaining.
block part2:
  var
    fields:seq[seq[string]] = @[]
    unconfirmed: int = ticket.len()
    total:BiggestInt = 1
  
  # Initially all possibilities exist
  for i in ticket.low() .. ticket.high():
    fields.add(@[])
    for name in rules.keys():
      fields[i].add(name)
      
  # From the valid tickets, eliminate possibilities based on values
  var
    updatedRules: seq[string]
    valid: bool
  
  for ticket in otherTickets:
    for i in fields.low() .. fields.high():
      if fields[i].len() == 1:
        continue
      
      updatedRules = @[]
      for name in fields[i]:
        valid = false
        for range in rules[name]:
          if ticket[i] >= range.low and ticket[i] <= range.high:
            valid = true
            break
          
        if valid:
          updatedRules.add(name)
      
      fields[i] = updatedRules
      if updatedRules.len() == 1: # Mapping confirmed
        unconfirmed -= 1
    
    if unconfirmed == 0: # All mappings were identified
      break
  
  # Resolve any pending assignments
  while unconfirmed > 0:
    for i in fields.low() .. fields.high():
      if fields[i].len() > 1:
        continue
      
      for j in fields.low() .. fields.high():
        if j == i or fields[j].len() == 1: 
          continue
        
        fields[j] = fields[j].filterIt(it != fields[i][0])
        if fields[j].len() == 1:
          unconfirmed -= 1
      
  # Calculate the product
  for i in fields.low() .. fields.high():
    if fields[i][0].len() > 9 and fields[i][0][0 .. 8] == "departure":
      total *= ticket[i]
    
  echo "Part 2: ", total
