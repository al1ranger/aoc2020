# https://adventofcode.com/2020/day/8

import os, strutils

if paramCount() < 1:
  quit(1)

type
  Instruction = tuple[opcode: string, argument, executionCount: int]

var
  program: seq[Instruction] = @[]
  accumulator, instructionPointer: int
  
proc resetExecutionState() =
  accumulator = 0
  instructionPointer = 0
  
  for i in program.low() .. program.high():
    program[i].executionCount = 0

proc executeCurrentInstruction() = 
  var
    instruction: int
  
  instruction = instructionPointer
  program[instruction].executionCount += 1
  
  case program[instruction].opcode:
    of "nop": # no operation
      instructionPointer += 1
    of "jmp": # jump
      instructionPointer += program[instruction].argument
    of "acc": # accumulator addition
      accumulator += program[instruction].argument
      instructionPointer += 1
    else:
      discard

# For simulation, we need to read the full program in advance
block readProgram:
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  var
    parts: seq[string]
  
  for line in data:
    parts = line.split(" ")
    program.add((opcode: parts[0], argument: parts[1].parseInt(), 
    executionCount: 0))
    
block part1:
  resetExecutionState()
  
  while program[instructionPointer].executionCount == 0:
    executeCurrentInstruction()
  
  echo "Part 1: ", accumulator

# Here, acc has no influence on jmp instructions. meaning that the jump 
# offset remains constant. So, any instruction executing more than once is an 
# indication of an infinite loop.
block part2:
  var
    originalInstruction: string
  
  for i in program.low() .. program.high():
    originalInstruction = program[i].opcode
    case program[i].opcode:
      of "jmp":
        program[i].opcode = "nop"
      of "nop":
        if program[i].argument != 0:
          program[i].opcode = "jmp"
        else:
          continue
      else:
        continue
    
    resetExecutionState()
    while instructionPointer <= program.high() and 
      program[instructionPointer].executionCount < 2:
      executeCurrentInstruction()
    
    if instructionPointer >= program.high(): # Success
      echo "Part 2: ", accumulator
      break part2
    else: # Undo change and try again
      program[i].opcode = originalInstruction