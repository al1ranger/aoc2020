# https://adventofcode.com/2020/day/4

import os, strutils, tables

if paramCount() < 1:
  quit(1)

let 
  required_attrs: string = "|byr|iyr|eyr|hgt|hcl|ecl|pid|" #cid is optional
  # required_attrs.len = 3 * required_count + (required_count + 1)
  required_count: int = int((required_attrs.len() - 1) / 4) 

type
  PassportDetails = Table[string, string]

var
  passportData: seq[PassportDetails] = @[]

block part1:
  let 
    # last blank line is required to mark the end of the record
    data = readFile(os.paramStr(1)).splitLines()

  var
    missing_fields, count_valid: int
    details: PassportDetails
    attrs: seq[string]
    keyval: seq[string]
  
  missing_fields = required_count
  count_valid = 0
  for line in data:
    if line.len() == 0: #blank line => start of new record
      # check status of last record and update count
      if missing_fields == 0:
        count_valid += 1
      
      passportData.add(details) # store record
      
      missing_fields = required_count
      details = initTable[string, string]()
      continue
    
    attrs = line.split(" ")
    for attr in attrs:
      keyval = attr.split(":")
      if keyval[0] in required_attrs:
        details[keyval[0]] = keyval[1] # store value
        
        missing_fields -= 1
    
  echo "Part 1: ", count_valid
  
block part2:
  let
    eyecolors: string = "|amb|blu|brn|gry|grn|hzl|oth|"
  
  var
    missing_fields, count_valid: int
    
  # validate according to the rules
  for detail in passportData:
    missing_fields = required_count
    
    for key, value in detail.pairs():
      case key:
        of "byr":
          try:
            var byr = value.parseInt()
            if byr >= 1920 and byr <= 2002:
              missing_fields -= 1
          except ValueError:
            discard
        of "iyr":
          try:
            var iyr = value.parseInt()
            if iyr >= 2010 and iyr <= 2020:
              missing_fields -= 1
          except ValueError:
            discard
        of "eyr":
          try:
            var eyr = value.parseInt()
            if eyr >= 2020 and eyr <= 2030:
              missing_fields -= 1
          except ValueError:
            discard
        of "hgt":
          try:
            var 
              hgt = value[0 ..< ^2].parseInt()
              unit = value[^2 .. ^1]
            
            case unit:
              of "cm":
                if hgt >= 150 and hgt <= 193:
                  missing_fields -= 1
              of "in":
                if hgt >= 59 and hgt <= 76:
                  missing_fields -= 1
          except ValueError:
            discard
        of "hcl":
          if value.len() == 7 and value.fromHex[:int]() > 0:
            missing_fields -= 1
        of "ecl":
          if value in eyecolors:
            missing_fields -= 1
        of "pid":
          try:
            var pid = value.parseInt()
            if value.len() == 9 and pid > 0:
              missing_fields -= 1
          except ValueError:
            discard
    
    if missing_fields == 0:
      count_valid += 1
    
  echo "Part 2: ", count_valid  