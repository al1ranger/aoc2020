# https://adventofcode.com/2020/day/23

import os, strutils

if paramCount() < 1:
  quit(1)

let
  data = paramStr(1).readFile().strip().splitLines()
    
type
  StartingArrangement = object
    min, max, head: int

# The problem description is a misdirection of sorts, as Part 1 immediately 
# hints at circular linked lists. Only in Part 2, it becomes obvious what a bad 
# choice that is.

# Array access is much faster than linked list or dicts. Also as cup labels are
# integers, arrays are a good choice. Furthermore, Nim also allows us to start
# array index from 1, which simplifies things.
# Here, for cup with label i, cups[i] contains the label of the next cup.
var
  cups: array[1 .. 10000000, int]

proc createStartingArangement(part2: bool = false):  StartingArrangement =
  result.min = 99999
  result.max = 0
  result.head = -1
  
  var
    no, prev: int
  
  # Data from file
  prev = 0
  for cup in data[0]:
    no = parseInt($cup)
    
    if no < result.min:
      result.min = no
    
    if no > result.max:
      result.max = no
    
    if prev > 0:
      cups[prev] = no
    else:
      result.head = no
    
    prev = no
    cups[prev] = no
    cups[no] = result.head # Close the loop
  
  if part2: # Extend to one million
    no = result.max + 1
    while no <= 1000000:
      result.max = no
      
      cups[prev] = no
      cups[no] = result.head # Close the loop
      
      prev = no
      no += 1
 
# Play the game and return the current cup at the end of the last turn
proc executeTurns(start: StartingArrangement, part2: bool = false): 
  int {.discardable.} =
  result = 0
  
  var
    currentCup, destinationCup, firstCup, lastCup, turns : int
    removed: seq[int]
    found: bool
  
  currentCup = start.head
  if part2:
    turns = 10000000
  else:
    turns = 100
  
  for turn in 1 .. turns:
    # Remove 3 cups
    firstCup = 0
    removed = @[]
    lastCup = currentCup
    for i in 1 .. 3:
      lastCup = cups[lastCup]
      removed.add(lastCup)
      if i == 1:
        firstCup = lastCup
      
    # Fix the links
    cups[currentCup] = cups[lastCup]
    cups[lastCup] = 0
    
    # Select the destination cup
    found = false
    destinationCup = currentCup
    while not found:
      destinationCup -= 1
      
      if destinationCup < start.min:
        destinationCup = start.max
      
      # Using the removed cups to determine the value is much faster
      if destinationCup notin removed:
        found = true
        break
    
    # Place the cups
    cups[lastCup] = cups[destinationCup]
    cups[destinationCup] = firstCup
    
    # Select the next cup
    currentCup = cups[currentCup]
    result = currentCup
  
block part1:
  var
    start: StartingArrangement
    cup: int
    value: string = ""
  
  start = createStartingArangement()
  start.executeTurns()
  
  # Get the sequence
  cup = 1
  while cups[cup] != 1:
    value = value & $cups[cup]
    cup = cups[cup]
    
  echo "Part 1: ", value

block part2:
  var
    start: StartingArrangement
    cup: int
    value: BiggestInt = 1
  
  start = createStartingArangement(true)
  start.executeTurns(true)
  
  # Get the product
  cup = 1
  for i in 1 .. 2:
    value = value * cups[cup]
    cup = cups[cup]
  
  echo "Part 2: ", value
