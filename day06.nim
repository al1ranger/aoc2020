# https://adventofcode.com/2020/day/6

import os, strutils, sequtils, sugar

if paramCount() < 1:
  quit(1)
  
proc toSet(s: string): set[char] =
  result = {}
  for letter in @s:
    result.incl(letter)
  
var
  responses: seq[seq[string]] = @[]
  
block part1: # unique answers in a group => set union
  let 
    # empty line at end is required to mark en of group
    data = readFile(os.paramStr(1)).splitLines()

  var
    group: seq[string] = @[]
    counts: seq[int] = @[]
  
  for line in data:
    if line.len() == 0: # end of record
      responses.add(group)
      
      var 
        allAnswers: set[char] = {}
      
      for response in group:
        allAnswers = allAnswers + response.toSet()
        
      counts.add(allAnswers.card())
      
      group = @[]
      continue
    
    group.add(line)
    
  echo "Part 1: ", counts.foldl(a + b)

block part2: # common answers in a group => set intersection
  # Answers given by all group members must be in the first member's response
  let counts = collect(newSeq):
    for group in responses:
      var
        allAnsweredYes: set[char] = group[group.low()].toSet()
        
      for response in group:
        allAnsweredYes = allAnsweredYes * response.toSet()
        
        if allAnsweredYes.card() == 0: # no point in continuing
          break
      
      allAnsweredYes.card()
      
  echo "Part 2: ", counts.foldl(a + b)