# https://adventofcode.com/2020/day/1

import os, strutils

if paramCount() < 1:
  quit(1)

let data = readFile(os.paramStr(1)).strip().splitLines()

block part1:
  var
    no1, no2: int
  for i in 0 ..< data.len() - 1:
    no1 = parseInt(data[i])
    for j in 1 ..< data.len():
      no2 = data[j].parseInt()
      
      if no1 + no2 == 2020:
        echo "Part 1: ", no1 * no2
        break part1
        
block part2:
  var
    no1, no2, no3: int
  for i in 0 ..< data.len() - 2:
    no1 = data[i].parseInt()
    for j in 1 ..< data.len() - 1:
      no2 = data[j].parseInt()
      
      no3 = 2020 - no1 - no2
      if no3 != no1 and no3 != no2 and $no3 in data:
        echo "Part 2: ", no1 * no2 * no3
        break part2