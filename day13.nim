# https://adventofcode.com/2020/day/13

import os, strutils

if paramCount() < 1:
  quit(1)
  
var
  busNumbers: seq[int] = @[]
  startTime: int
  
block part1:
  let
    data = paramStr(1).readFile().strip().splitLines()
  
  startTime = data[0].parseInt()
 
  var
    busNo, delay, minimumDelay, chosenBus: int
  
  busNo = 0
  delay = 0
  minimumDelay = 9999999
  chosenBus = 0
  for bus in data[1].split(","):
    if bus[0] == 'x':
      busNumbers.add(-1)
      continue
  
    busNo = bus.parseInt()
    busNumbers.add(busNo)
    
    # Waiting time is the time required for the current trip to complete. This
    # is the difference between the time elapsed (remainder of starting time 
    # divided by trip time) and the trip time (bus number)
    delay = busNo - (startTime mod busNo)
    
    if delay < minimumDelay:
      minimumDelay = delay
      chosenBus = busNo
  
  echo "Part 1: ", minimumDelay * chosenBus

# Here, we need to use the Chinese Remainder Theorem, as explained in
# https://www.reddit.com/r/adventofcode/comments/kc4njx/2020_day_13_solutions/gfnbwzc
# As mentioned in the Wikipedia article, we will use seiving to find the value.
# https://en.wikipedia.org/wiki/Chinese_remainder_theorem
# Brief explanation:
# To apply Chinese Remainder Theorem, the equations need to be in the form
# x mod n = a
# The equations in the problem are of the form (t + i) mod n = 0, which can be
# rewritten as t mod n = -i mod n
# if i is not 0, -i mod n means n - (i mod n) 
block part2:
  var
    n: seq[BiggestInt] = @[]
    a: seq[BiggestInt] = @[]
    nIndex, remainder, timeStamp, increment: BiggestInt
    
  for i in busNumbers.low() .. busNumbers.high():
    if busNumbers[i] < 0:
      continue
    
    n.add(busNumbers[i])
    if i == 0:
      a.add(0)
    else:
      a.add(busNumbers[i] - i mod busNumbers[i])
  
  # For performance reasons, sort in descending order
  for i in n.low() .. n.high():
    for j in i + 1 .. n.high():
      if n[j] > n[i]:
        swap(n[i], n[j])
        swap(a[i], a[j])
  
  increment = n[0]
  nIndex = 1
  timeStamp = a[0]
  while nIndex <= n.high():
    remainder = timeStamp mod n[nIndex]
    
    if remainder == a[nIndex]:
      increment *= n[nIndex]
      nIndex += 1
    else:
      timeStamp += increment
    
  echo "Part 2: ", timeStamp