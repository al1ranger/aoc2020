# https://adventofcode.com/2020/day/18
# To be refactored and cleaned up
import os, strutils

if paramCount() < 1:
  quit(1)

proc doOperation(lhs, rhs:string, operation: char):BiggestInt

proc calc(lhs, rhs: BiggestInt, operand: char): BiggestInt =
  result = 0
  
  case operand
  of '*':
    result = lhs * rhs
  of '+':
    result = lhs + rhs
  else:
    discard

proc evaluate(exp: string, operators: seq[char] = @['+', '*']): string = 
  result = ""
  
  if exp.len() == 0:
    return
  
  var
    index:int = 0
    calculated:string = exp#.replace(" ")
    original: string
  
  while calculated.find("(") > -1: 
    index = 0
    while index < calculated.len():
      if calculated[index] in Whitespace:
          index += 1
          continue
      
      case calculated[index]
      of '(':
        var
          subexpr: string = ""
          level: int = 1
        
        index += 1
        while level > 0:
          if calculated[index] == ')':
            if level > 1:
              subexpr.add(calculated[index])
            
            level -= 1
          elif level > 0:
            subexpr.add(calculated[index])
            if calculated[index] == '(':
              level += 1
          
          index += 1
        
        original = "(" & subexpr & ")"
        subexpr = subexpr.evaluate(operators)
        if subexpr.find("*") > -1 or subexpr.find("+") > -1:
          subexpr = "( " & subexpr & " )"
        calculated = calculated.replaceWord(original, subexpr)
        index = calculated.len()
        break
      else:
        discard
      
      index += 1
  
  # Now only a simple exprssion remains
  var
    lhs, rhs: string
    found: bool
  
  for operator in operators:
    #echo "op: ", operator
    while calculated.find(operator) > -1:
      index = 0
      lhs = ""
      rhs = ""
      found = false
      
      while index < calculated.len():
        if calculated[index] in Whitespace:
          index += 1
          continue
          
        if calculated[index].isDigit():
          if found:
            rhs.add(calculated[index])
          else:
            lhs.add(calculated[index])
        elif not found and calculated[index] == operator:
          found = true
        else:
          if found:
            calculated = calculated.replaceWord(lhs & " " & operator & " " & rhs,
            $(calc(lhs.parseBiggestInt(), rhs.parseBiggestInt(), operator)))
            index = -1
            #break
          
          lhs = ""
          rhs = ""
          found = false
        
        index += 1
      
      if found:
        var
          r: BiggestInt
        r =  calc(lhs.parseBiggestInt(), rhs.parseBiggestInt(), operator)
        calculated = calculated.replaceWord(lhs & ' ' & operator & ' ' & rhs, $(r))
        
  # Now only the final result remains
  result = calculated
    
proc evaluateExpression(exp:string):BiggestInt =
  result = 0
  
  if exp.len() == 0:
    return
  
  var
    lhs, rhs: string
    operation: char = '_'
    level: int = 0
  
  for token in exp:
    case token
    of Whitespace:
      continue
    of '(':
      level += 1
      if level == 1 and operation == '_':
        continue
    of '+', '*':
      if level == 0:
        if operation == '_':
          operation = token
          continue
        else:
          lhs = $(lhs.doOperation(rhs, operation))
          operation = token
          rhs = ""
          continue
    of ')':
      if level == 1 and operation == '_':
        continue
      level -= 1
    else:
      discard
      
    if operation == '_':
      lhs.add(token)
    else:
      rhs.add(token)
  
  result = lhs.doOperation(rhs, operation)

proc doOperation(lhs, rhs:string, operation: char):BiggestInt =
  result = 0
  
  case operation:
  of '+':
    result = lhs.evaluateExpression() + rhs.evaluateExpression()
  of '*':
    result = lhs.evaluateExpression() * rhs.evaluateExpression()
  else: # only lhs will have value
    if lhs.find({'(', '*', '+', ')'}) >= 0:
      result = lhs.evaluateExpression()
    else:
      result = lhs.parseBiggestInt()

proc calculateSum():BiggestInt =
  result = 0
    
  for line in paramStr(1).readFile().strip().splitLines():
    result += line.evaluateExpression()

block part1:
  echo "Part 1: ", calculateSum()

block part2:
  var
    sum: BiggestInt = 0
    value:BiggestInt
  
  let 
    input = paramStr(1).readFile().strip().splitLines()
  for i in input.low() .. input.high():
    value = input[i].evaluate().parseBiggestInt()
    sum += value
  
  echo "Part 2: ", sum