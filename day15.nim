# https://adventofcode.com/2020/day/15

import os, strutils, tables

if paramCount() < 1:
  quit(1)
  
let
  input = paramStr(1).readFile.strip().splitLines()[0]

# Calculate number after N turns. These links are useful for getting an idea 
# of the mathematics involved.
# https://www.reddit.com/r/adventofcode/comments/kdf85p/2020_day_15_solutions/
# https://codegolf.stackexchange.com/questions/186654/nth-term-of-van-eck-sequence
# No need to keep the entire turn history
proc calculateNumber(turns: int): int =
  var
    numbers: Table[int, int] = initTable[int, int]()
    start: seq[string]
    turn: int = 0
    currentNum: int
    
  start = input.split(",")
  for i in start.low() .. start.high():
    var
      no: int
    
    no = start[i].parseInt()
    turn = i + 1
    currentNum = no
    
    numbers[no] = turn
  
  while turn <= turns:
    turn += 1
    result = currentNum
    
    if result notin numbers:
      currentNum = 0
    else:
      currentNum = turn - 1 - numbers[result]
    
    numbers[result] = turn - 1
  
block part1:
  echo "Part 1:", 2020.calculateNumber()

# This is same as Part 1 with more turns  
block part2:
  echo "Part 2:", 30000000.calculateNumber()