# https://adventofcode.com/2020/day/12

import os, strutils, math

if paramCount() < 1:
  quit(1)
  
var
  instructions: seq[string] = @[]

# Here, F refers to the movement along the angle with the positive X-axis. 
# L and R refer to the angle of the ship with the positive X-axis.
# For the ship, the origin of the coordinate system is its starting position.
block part1:
  var
    shipAngle: float
    shipX, shipY, value: int
  
  shipX = 0
  shipY = 0
  shipAngle = 0
  for line in paramStr(1).readFile().strip().splitLines():
    instructions.add(line)
  
    value = line[1.. ^1].parseInt()
    
    case line[0]:
      of 'E':
        shipX += value
      of 'W':
        shipX -= value
      of 'N':
        shipY += value
      of 'S':
        shipY -= value
      of 'L':
        shipAngle += value.float()
      of 'R':
        shipAngle -= value.float()
      of 'F':
        shipX += round(cos(shipAngle.degToRad()) * value.float()).int()
        shipY += round(sin(shipAngle.degToRad()) * value.float()).int()
      else:
        discard
    
  echo "Part 1: ", abs(shipX) + abs(shipY)

# Here, F refers to the movement of the ship along the angle made with its
# waypoint. L and R refer to the rotation of the waypoint around the ship.
# For the waypoint, the origin of the coordinate system is the ship's position.
# For the ship, the origin of the coordinate system is its starting position.
block part2:
  type
    Position = tuple[x, y: int]
  
  proc rotate(position: var Position, degrees: int) =
    var
      hypotenuse, angle: float
    
    angle = arctan2(position.y.float(), position.x.float()) +
      degrees.float().degToRad()
    
    hypotenuse = hypot(position.x.float(), position.y.float())
    position.x = round(hypotenuse * cos(angle)).int()
    position.y = round(hypotenuse * sin(angle)).int()

  var
    wayPoint: Position = (x: 10, y: 1)
    value, shipX, shipY: int
  
  shipX = 0
  shipY = 0
  for line in instructions:
    value = line[1.. ^1].parseInt()
    
    case line[0]:
      of 'E':
        wayPoint.x += value
      of 'W':
        wayPoint.x -= value
      of 'N':
        wayPoint.y += value
      of 'S':
        wayPoint.y -= value
      of 'L':
        wayPoint.rotate(value)
      of 'R':
        wayPoint.rotate(-1 * value)
      of 'F':
        shipX += value * wayPoint.x
        shipY += value * wayPoint.y
      else:
        discard
  
  echo "Part 2: ", abs(shipX) + abs(shipY)