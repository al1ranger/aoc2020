# https://adventofcode.com/2020/day/20

import os, strutils, algorithm, tables

if paramCount() < 1:
  quit(1)

type
  #Edge = enum
  #  left, right, top, bottom
  TileRow = array[10, char]
  TileMap = array[10, TileRow]
  Tile = object
    number: int
    data: TileMap

# Flip about horizontal axis
#proc flip(tile: Tile):Tile =
#  result.number = tile.number
#  for i in 0 .. 9:
#    result.data[9 - i] = tile.data[i]

# Counterclockwise rotation
#proc rotate(tile: Tile, times: int = 1): Tile =
#  result.number = tile.number
  
#  var
#    repeat:int = times
  
#  if repeat < 1:
#    repeat = 1
  
#  while repeat > 0:
#    repeat -= 1
    
#    for i in 0 .. 9:
#      for j in 0 .. 9:
#        result.data[9 - j][i] = tile.data[i][j]

#proc placeAt(lhs: Tile, edge: Edge, rhs: var Tile): bool =
#  var
#    flips:int = 2
#    rotations:int = 3
  
#  result = false
  
#  block matchEdges:
#    while flips > 0:  
#      while rotations > 0:
#        result = true
        
#        case edge
#        of left:
#          for i in 0 .. 9:
#            if lhs.data[i][0] != rhs.data[i][9]:
#              result = false
#              break
#        of right:
#          for i in 0 .. 9:
#            if lhs.data[i][9] != rhs.data[i][0]:
#              result = false
#              break
#        of top:
#          for i in 0 .. 9:
#            if lhs.data[0][i] != rhs.data[9][i]:
#              result = false
#              break
#        of bottom:
#          for i in 0 .. 9:
#            if lhs.data[9][i] != rhs.data[0][i]:
#              result = false
#              break
            
#        if result:
#          break matchEdges
        
#        rotations -= 1
#        rhs = rhs.rotate()
    
#      flips -= 1
#      rhs = rhs.flip()

proc hasMatchingEdge(lhs, rhs: Tile): bool =
  result = false
  
  var
    lhsEdges: seq[string] = @[]
    rhsEdges: seq[string] = @[]
    
  # Top
  lhsEdges.add(lhs.data[0].join())
  rhsEdges.add(rhs.data[0].join())
  
  # Bottom
  lhsEdges.add(lhs.data[9].join())
  rhsEdges.add(rhs.data[9].join())
  
  # Left and right
  for i in 0 .. 9:
    if i == 0:
      lhsEdges.add("")
      lhsEdges.add("")
      rhsEdges.add("")
      rhsEdges.add("")
    
    lhsEdges[^2].add(lhs.data[i][0])
    lhsEdges[^1].add(lhs.data[i][9])
    rhsEdges[^2].add(rhs.data[i][0])
    rhsEdges[^1].add(rhs.data[i][9])
    
  for lEdge in lhsEdges:
    for rEdge in rhsEdges:
      if lEdge == rEdge or lEdge == rEdge.reversed() or lEdge.reversed() == rEdge:
        result = true
        return

proc calculateSharedEdges(tiles: seq[Tile]): Table[int, seq[int]] =
  result = initTable[int, seq[int]]()
  
  for tile in tiles:
    result[tile.number] = @[]
    
    for otherTile in tiles:
      if otherTile.number == tile.number:
        continue
      
      if tile.hasMatchingEdge(otherTile):
        result[tile.number].add(otherTile.number)

#proc print(tile: Tile) = 
#  echo "Tile ", tile.number
#  for i in 0 .. 9:
#    echo tile.data[i].join()

var
  tiles: seq[Tile] = @[]

block readFile:
  var
    tile: Tile = Tile()
    lineNo: int
  
  for line in paramStr(1).readFile().splitLines():
    if line.len() == 0:
      if tile.number > 0:
        tiles.add(tile)
        tile = Tile()
      continue
  
    if line[0] == 'T':
      tile.number = line[5 .. 8].parseInt()
      lineNo = 0 
    else:
      for i in 0 ..< 10:
        tile.data[lineNo][i] = line[i]
      lineNo += 1
  
block part1:
  var
    product:BiggestInt = 1
  for tileNumber, details in tiles.calculateSharedEdges().pairs():
    if details.len() == 2:
      product *= tileNumber
  echo "Part 1: ", product