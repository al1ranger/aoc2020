# https://adventofcode.com/2020/day/9

import os, strutils, deques, algorithm

if paramCount() < 1:
  quit(1)

const
  PreambleLength: int = 25

var
  input: seq[BiggestInt] = @[]
  invalid: BiggestInt = 0

# Here, it is better to read the complete data before prcessing
block readInput:
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  for line in data:
    input.add(line.parseBiggestInt())
    
block part1:
  var
    preamble: Deque[int64] = initDeque[BiggestInt]()
    
  for number in input:
    var
      number1, number2: BiggestInt
      
    if preamble.len() > PreambleLength: # Remove unwanted
      preamble.popFirst()
    
    if preamble.len() == PreambleLength: # Complete  
      # Check validity
      number1 = 0
      number2 = 0
      for value in preamble:
        number1 = value
        number2 = number - value
        
        # Must be a different number from the preamble
        if number1 != number2 and number2 in preamble:
          break
      
        number1 = 0
        number2 = 0
      
      if number1 == number2:
        echo "Part 1: ", number
        invalid = number
        break
    
    preamble.addLast(number) 
    
block part2:
  var
    components: seq[BiggestInt]
    balance: BiggestInt
  
  for start in input.low() .. input.high() - 2:
    components = @[]
    balance = invalid
    
    for i in start .. input.high():
      components.add(input[i])
      balance = balance - input[i]
      
      if balance <= 0:
        break
      
    if balance == 0: # Exact match is found
      components.sort()
      break
  
  echo "Part 2: ", components[0] + components[^1]
