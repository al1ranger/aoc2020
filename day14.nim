# https://adventofcode.com/2020/day/14

import os, strutils, sugar, tables

if paramCount() < 1:
  quit(1)

# For this problem, Nim's Procedural types 
# (https://nim-lang.org/docs/manual.html#types-procedural-type) can be used
# to separate out the logic specific to the two parts
type
  Instruction = tuple[lhs, rhs: string]
  Memory = tuple[data: Table[BiggestInt, BiggestInt], mask: string]
  UpdateMemory = proc (memory: var Memory, address, value: BiggestInt)

# Initializes the chip's memory
proc initMemory(memory: var Memory) =
  memory.mask = ""
  memory.data = initTable[BiggestInt, BiggestInt]()

# Executes a single instruction
proc executeInstruction(memory: var Memory, instruction: Instruction, 
  updateMemory: UpdateMemory) =
  case instruction.lhs[0 .. 3]
  of "mask":
    memory.mask = instruction.rhs
  of "mem[":
    var
      address, value: BiggestInt
    
    address = instruction.lhs[4 .. ^2].parseBiggestInt()
    value = instruction.rhs.parseBiggestInt()
    
    memory.updateMemory(address, value)
  else:
    discard

# Read input
let program = collect(newSeq):
  for line in paramStr(1).readFile().strip().splitLines():
    var
      data: seq[string]
    
    data = line.split(" = ")
    
    (lhs: data[0], rhs: data[1])

# Executes the program and returns the total
# updateMemory is a callback procedure used to update the value in the memory
proc executeProgram(updateMemory: UpdateMemory): BiggestInt = 
  var
    memory: Memory
  
  result = 0
  memory.initMemory()
  
  for instruction in program:
    memory.executeInstruction(instruction, updateMemory)
    
  for address, value in memory.data.pairs():
    result += value

# Overlay mask on the value, ignoring X
block part1:
  proc updateMemory(memory: var Memory, address, value: BiggestInt) =
    var
      bits: string
    # Apply the mask
    bits = value.toBin(36)
    for i in 0 ..< memory.mask.len():
      if memory.mask[i] == 'X':
        continue
      
      bits[i] = memory.mask[i]
    
    memory.data[address] = 0
    memory.data[address] = bits.fromBin[:BiggestInt]()
  
  echo "Part 1: ", updateMemory.executeProgram()

# Overlay mask on the address, ignoring 0 and treating X as wildcard 
block part2:
  # Generates all possible addresses
  proc expandAddress(address: string, allAddresses: var seq[string]) =
    const
      replacements: seq[char] = @['0', '1']
    
    var
      index: int
      currentAddress: string
      
    index = address.find('X')
    
    if index == -1: # Expansion complete
      allAddresses.add(address)
    else:
      currentAddress = address
      for bit in replacements:
        currentAddress[index] = bit
        currentAddress.expandAddress(allAddresses)
        
  proc updateMemory(memory: var Memory, address, value: BiggestInt) =
    var
      bits: string
      addresses: seq[string] = @[]
    
    # Apply the mask
    bits = address.toBin(36)
    for i in 0 ..< memory.mask.len():
      if memory.mask[i] == '0':
        continue
      
      bits[i] = memory.mask[i]
    
    bits.expandAddress(addresses)
    for addr in addresses:
      memory.data[addr.fromBin[:BiggestInt]()] = value
  
  echo "Part 2: ", updateMemory.executeProgram()