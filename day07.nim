# https://adventofcode.com/2020/day/7

import os, strutils

if paramCount() < 1:
  quit(1)
  
const
  TheColor: string = "shiny gold"

var
  # containerMap is a square matrix where row corresponds to a color and 
  # columns denote bags of other colors contained in this colored bag
  # containerKeys contains the index for each color in the map
  containerMap: seq[seq[int]] = @[]
  containerKeys: seq[string] = @[]

proc getIdForColor(color: string): int = 
  if color == "no other":
    result = -1
    return result
  
  result = containerKeys.find(color)
  if result < 0:
    containerKeys.add(color)
    containerMap.add(@[])
    
    result = containerKeys.len() - 1
    
    # Adjust the 2d array if required
    for i in containerKeys.low() .. containerKeys.high():
      while containerMap[i].len() < containerKeys.len():
        containerMap[i].add(0)
  
block part1:
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
  
  var
      parts, colornames: seq[string]
      containerColor, containedColor: int
    
  for line in data:
    parts = line.split(" contain ")
    
    colornames = parts[0].split(" ")
    containerColor = @[colornames[0], colornames[1]].join(" ").getIdForColor()
    
    if containerColor == -1:
      continue
      
    for bag in parts[1].split(", "):
      if bag[0 .. 7] == "no other":
        continue
      
      colornames = bag.split(" ")
      containedColor = @[colornames[1], colornames[2]].join(" ").getIdForColor()
      
      if containedColor == -1:
        continue
      
      # Set the number of bags contained
      containerMap[containerColor][containedColor] = colornames[0].parseInt()
  
  let
    shinyGold: int = TheColor.getIdForColor()
  
  var
    containers, alternatives: seq[int]
    container: int
    
  # Start with the shiny gold bag
  containers.add(shinyGold)
  
  # Locate parents, till the top level bags
  alternatives = @[]
  while containers.len() > 0:
    container = containers.pop()
    
    for i in containerKeys.low() .. containerKeys.high():
      if containerMap[i][container] > 0:
        containers.add(i)
    
    if container != shinyGold and container notin alternatives: 
      alternatives.add(container) # only colors are important
  
  echo "Part 1: ", alternatives.len()

# In some ways, this is a reverse of part 1, as we need all bags within
# Here we also need to keep track of the quantites, to calculate the total
block part2:
  type
    BagRequired = tuple[color, count: int]
  
  let
    shinyGold: int = TheColor.getIdForColor()
  
  var
    contained: seq[BagRequired]
    container: BagRequired
    bagsRequired: seq[BagRequired]
    
  # Start wth the shiny gold bag
  contained.add((color: TheColor.getIdForColor(), count: 1))
  
  # Locate all descendants, keep track of their quantities
  while contained.len() > 0:
    container = contained.pop()
    
    for i in containerKeys.low() .. containerKeys.high():
      if containerMap[container.color][i] > 0:
        contained.add((color: i, count: containerMap[container.color][i] * container.count))
    
    bagsRequired.add(container)
  
  # Calculate the total number of bags
  var
    total: int = 0
  
  for bag in bagsRequired:
    if bag.color != shinyGold:
      total += bag.count
  
  echo "Part 2: ", total